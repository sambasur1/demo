package com.test;

import java.util.*;
import java.util.stream.Collectors;

class TestVO{
	int id;
	String name;
	
	public TestVO(int id, String name) {
		this.id = id;
		this.name = name;
	}

	@Override
	public String toString() {
		return "TestVO [id=" + id + ", name=" + name + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		TestVO vo = (TestVO) obj;
				
		if(this.id == vo.id && this.name.equals(vo.name)) {
			System.out.println("Equal");
			return true;
		}
		System.out.println("Not Equal");
		return false;
	}
	
	 @Override
	    public int hashCode()
	    {
		 
	        return this.id + this.name.hashCode();
	    }
}

public class Test1 {
	public static void main(String args[]) {
		List<TestVO> list = Arrays.asList(new TestVO(1, "Sam"), new TestVO(2, "Raj"), new TestVO(1, "Sam"));
		System.out.println(list);
		//Map<TestVO, TestVO> map = list.stream().collect(Collectors.toMap(obj -> obj, obj -> obj));
		Map<TestVO, TestVO> map = new HashMap<>();
		for(TestVO vo : list) {
			map.put(vo, vo);
		}
		System.out.println(map);
	}
}
