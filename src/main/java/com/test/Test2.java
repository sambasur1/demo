package com.test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Test2 {
	
	public static void main(String args[]) {
		String s = "HelloWorld";
		String[] array = s.split("");
		List<String> list = Arrays.asList(array);
		
		System.out.println(list);
		//now you apply stream filter etc
		List<String> distinct = list.stream().distinct().collect(Collectors.toList());
		System.out.println(distinct);
	}

}
