package com.test;


import java.util.*;
import java.util.stream.Collectors;

class Employ{
	int no;
	String name;
	double sal;
	public Employ(int no, String name, double sal) {
		super();
		this.no = no;
		this.name = name;
		this.sal = sal;
	}
	@Override
	public String toString() {
		return "Employ [no=" + no + ", name=" + name + ", sal=" + sal + "]";
	}
	public int getNo() {
		return no;
	}
	public String getName() {
		return name;
	}
	public double getSal() {
		return sal;
	}
	
	
	
}
public class Test4 {

	public static void main(String args[]) {
		
		List<Employ> list = Arrays.asList(new Employ(1, "test1", 5000), 
				new Employ(2, "test2", 3000), new Employ(3, "test3", 3500),
				new Employ(4, "test4", 2500));
		
		List<Employ> sortedList = list.stream().sorted(Comparator.comparingDouble(Employ::getSal).reversed()).collect(Collectors.toList());
		System.out.println(sortedList);
		
		//second highest employ
		System.out.println(sortedList.get(1));
		
		//second highest employ salary
		System.out.println(sortedList.get(1).getSal());
		
		
	}
}
