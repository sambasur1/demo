package com.example.demo.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.EmpService;
import com.example.demo.service.UserService;
import com.example.demo.vo.Account;
import com.example.demo.vo.Address;
import com.example.demo.vo.Employ;
import com.example.demo.vo.User;
import com.example.demo.vo.UserList;

@RestController
public class TestController {

	@GetMapping("/getEmploys")
	public List<Employ> getEmploys() {
		
		List<Employ> empList = new ArrayList<>();
		Employ e1 = new Employ();
		e1.setEno(1);
		e1.setEname("Jacob");
		e1.setEsalary(10000);
		
		List<Address> alList = new ArrayList<>();
		Address a1 = new Address();
		a1.setType("Primary");
		a1.setCity("Dallas");
		a1.setPincode("75034");
		alList.add(a1);
		e1.setAdressList(alList);
		
		Account account = new Account();
		account.setAccountNo("12345");
		account.setIfsc("BOA-2333");
		e1.setAccount(account);
		empList.add(e1);
		System.out.println("Inside GET :" + empList);
		return empList;		
	}
	
	@PostMapping("/postEmploys")
	public List<Employ> postEmploys(@RequestBody  List<Employ> empList) {
		empList.get(0).setEname("Matt");
		empList.get(0).getAdressList().get(0).setCity("Las Vegas");
		System.out.println("Inside Post :" + empList);
		return empList;		
	}
}
