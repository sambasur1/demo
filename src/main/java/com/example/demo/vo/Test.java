package com.example.demo.vo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Test {
	public static void main(String args[]) throws JsonProcessingException {
		Account account = new Account();
		account.setAccountNo("1111");
		account.setEno(100);
		account.setId(5);
		account.setIfsc("000435");
		
		System.out.println(account);
		ObjectMapper objectMapper = new ObjectMapper();
		
		//Object to String
		String json = objectMapper.writeValueAsString(account);		
		System.out.println(json);
		
		//String to object
		String json1 = "{\"id\":6,\"accountNo\":\"2222\",\"ifsc\":\"000465\",\"eno\":100}";
		
		Account account1 = objectMapper.readValue(json1, Account.class);
		System.out.println("Account1:" + account1);
		System.out.println("Account1 ifsc :" + account1.getIfsc());
		
	}
}
